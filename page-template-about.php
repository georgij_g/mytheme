<?php 
/* 
    Template Name: About Us Page Template - 1 
*/
// Call Header
get_header(); 
?>
    <section class="hero-slider">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
            <?php
            $counter = 0;
            $args = array(
                'post_type'      => 'carousel',
                'posts_per_page' => 5,
                'author_name'    => 'petko',
                'orderby' => 'title',
	            'order'   => 'ASC',
            );
            $loop = new WP_Query($args);
            // echo json_encode($loop);
            while ( $loop->have_posts() ) {
                $loop->the_post();
                if(get_field('is_slide_visible')) {
                ?>
                <div class="carousel-item <?php if( $counter == 0) { echo "active"; } ?> text-center">
                    <h1>Slide Title: <?php echo get_the_title(); ?></h1>
                        <?php echo get_the_post_thumbnail(); ?>
                    <h1>Ova e Counter: <?php echo $counter; ?></h1>
                </div>
                <?php
                $counter++;
                }

               

                
                
            }
            wp_reset_query();            
            ?>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>


    <section class="page-content container">
        <h1>Page Title: <?php echo get_the_title(); ?></h1>
        Page Content: <?php echo the_content(); ?>
    </section>

    <section class="about-content container">
        <hr />
        <h1>ACF Content</h1>
        <hr />
            <h1>Tuka e naslovot: 
                <?php
                // Go vmetnuvame vo promenliva.
                $slogan_na_alkaloid = get_field('title');
                // Sekogash koga "pobaruvame" nekoi custom fields, 
                // dobra navika e da proverime dali voopshto ima sodrzhina.
                if($slogan_na_alkaloid) {
                    echo $slogan_na_alkaloid;
                } else {
                    echo "Администраторот заборавил да стави содржина.";
                }
                ?>
            </h1>
            <div>
                <?php 
                the_field('section_1_text');
                ?>
            </div>
    </section>
   

<?php 
// Call Footer
get_footer(); 
?>
