<!-- Оваа функција го повикува header.php фајлот од вашата тема -->
<?php 
// Call Header
get_header(); 
?>

<?php 
	// if($scenarioA) {
	// 	// izvrshi se shto treba dokolku uslovot e ispolnet
	// } else if($scenarioB) {
	// 	// izvrshi se shto treba dokolku uslovot e ispolnet
	// } else if($scenarioC) {
	// 	// izvrshi se shto treba dokolku uslovot e ispolnet
	// } else {
	// 	// izvrshi shto e default -
	// }
?>


<?php 

	if (have_posts()) {
		while(have_posts()) {
			the_post();
			?>
				<h3>
					<?php the_title(); ?>
				</h3>
			<?php

			?>
				<div class="post-content">
					<?php
						echo the_content();
					?>
				</div>
				<div class="read-more">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

						<strong>Read Post!</strong>

					</a>


				</div>
			<?php
		}
	} else {
		?>
		<p>No posts found. :(</p>
		<?php
	}
?>


<?php 
//Call Footer
get_footer(); 
?>